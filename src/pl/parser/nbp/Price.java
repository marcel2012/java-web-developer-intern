package pl.parser.nbp;

final class Price {
    private Float buyPrice;
    private Float sellPrice;

    Price(Float buyPrice, Float sellPrice) {
        this.buyPrice = buyPrice;
        this.sellPrice = sellPrice;
    }

    Float getBuyPrice() {
        return buyPrice;
    }

    void setBuyPrice(Float buyPrice) {
        this.buyPrice = buyPrice;
    }

    Float getSellPrice() {
        return sellPrice;
    }

    void setSellPrice(Float sellPrice) {
        this.sellPrice = sellPrice;
    }
}
