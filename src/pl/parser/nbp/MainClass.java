package pl.parser.nbp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

final public class MainClass {
    private MainClass() {
    }

    public static void main(String[] args) {
        if (args.length < 3)
            throw new IllegalArgumentException("Too few arguments");
        String currency = args[0];
        Pattern currencyPattern = Pattern.compile("^[A-Z]{3}$");
        Matcher currencyMatcher = currencyPattern.matcher(currency);
        if (!currencyMatcher.find())
            throw new IllegalArgumentException("Bad currency code");

        Date startDate, endDate;
        try {
            startDate = stringToDate(args[1]);
            endDate = stringToDate(args[2]);
        } catch (ParseException exception) {
            throw new IllegalArgumentException("Error parsing date", exception);
        }
        if (startDate.after(endDate))
            throw new IllegalArgumentException("End date can't be before start date");
        if (endDate.after(new Date()))
            throw new IllegalArgumentException("End date can't be in the future");

        try {
            NbpApiClient nbpApiClient = new NbpApiClient(startDate, endDate, currency);
            List<Price> priceList = nbpApiClient.getPriceList();

            List<Float> buyPrices = priceList.stream().map(Price::getBuyPrice).collect(Collectors.toList());
            List<Float> sellPrices = priceList.stream().map(Price::getSellPrice).collect(Collectors.toList());
            Float averageBuyPrice = ExtendedMath.averageValue(buyPrices);
            Float standardDeviation = ExtendedMath.standardDeviation(sellPrices);

            System.out.println(String.format("%.4f\n%.4f", averageBuyPrice, standardDeviation));
        } catch (Exception exception) {
            System.err.println("Can't download price data");
            exception.printStackTrace();
        }
    }

    private static Date stringToDate(String str) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.parse(str);
    }
}
