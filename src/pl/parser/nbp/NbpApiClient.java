package pl.parser.nbp;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class NbpApiClient {
    private final static String apiURL = "http://www.nbp.pl/kursy/xml/";

    private Date startDate, endDate;
    private String currency;

    NbpApiClient(Date startDate, Date endDate, String currency) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.currency = currency;
    }

    private static Price getPriceFromXmlData(String xml, String currency) throws ParserConfigurationException, XPathExpressionException, IOException, SAXException {
        Document xmlDocument = parseXmlDocument(xml);
        Element currencyRecord = findXmlRecord(xmlDocument, currency);
        if (currencyRecord != null) {
            Float divisor = Float.parseFloat(currencyRecord.getElementsByTagName("przelicznik").item(0).getTextContent().replace(",", "."));
            Float buyPrice = Float.parseFloat(currencyRecord.getElementsByTagName("kurs_kupna").item(0).getTextContent().replace(",", ".")) / divisor;
            Float sellPrice = Float.parseFloat(currencyRecord.getElementsByTagName("kurs_sprzedazy").item(0).getTextContent().replace(",", ".")) / divisor;
            return new Price(buyPrice, sellPrice);
        } else
            throw new NullPointerException("No data for selected currency");
    }

    private static Calendar dateToCalendar(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    private static String getFileName(String list, String date) {
        Pattern xmlFilenamePattern = Pattern.compile("(c[0-9]{3}z" + date + ")");
        Matcher matcher = xmlFilenamePattern.matcher(list);
        if (matcher.find())
            return matcher.group(1) + ".xml";
        else
            return null;
    }

    private static Element findXmlRecord(Document xmlDocument, String currency) throws XPathExpressionException {
        XPath findCurrency = XPathFactory.newInstance().newXPath();
        return (Element) findCurrency.compile("//tabela_kursow/pozycja[kod_waluty/text()='" + currency + "']").evaluate(xmlDocument, XPathConstants.NODE);
    }

    private static Document parseXmlDocument(String xml) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = factory.newDocumentBuilder();
        return documentBuilder.parse(new InputSource(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8))));
    }

    List<Price> getPriceList() throws IOException {
        Calendar startCalendar = dateToCalendar(startDate);
        Calendar endCalendar = dateToCalendar(endDate);
        Integer actualYear = dateToCalendar(new Date()).get(Calendar.YEAR);

        Integer lastDownloadedYear = -1;
        String fileList = null;
        List<Price> priceList = new ArrayList<>();

        while (!startCalendar.after(endCalendar)) {
            Integer year = startCalendar.get(Calendar.YEAR);
            Integer month = startCalendar.get(Calendar.MONTH) + 1;
            Integer day = startCalendar.get(Calendar.DAY_OF_MONTH);
            String selectedDate = String.format("%02d%02d%02d", year % 2000, month, day);

            if (!year.equals(lastDownloadedYear)) {
                fileList = HTTPDownloader.downloadUrl(apiURL + "dir" + (year.equals(actualYear) ? "" : year) + ".txt");
                lastDownloadedYear = year;
            }

            String fileName;
            if ((fileName = getFileName(fileList, selectedDate)) != null) {
                String xmlData = HTTPDownloader.downloadUrl(apiURL + fileName);
                try {
                    priceList.add(getPriceFromXmlData(xmlData, currency));
                } catch (Exception exception) {
                    throw new IOException("Parsing data error", exception);
                }
            }
            startCalendar.add(Calendar.DATE, 1);
        }
        return priceList;
    }
}
