package pl.parser.nbp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

final class HTTPDownloader {
    private HTTPDownloader() {
    }

    static String downloadUrl(String url) throws IOException {
        URL fileURL = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) fileURL.openConnection();
        connection.setRequestMethod("GET");
        StringBuilder output = new StringBuilder();
        try (InputStreamReader inputStreamReader = new InputStreamReader(connection.getInputStream());
             BufferedReader reader = new BufferedReader(inputStreamReader)) {
            String line;
            while ((line = reader.readLine()) != null)
                output.append(line);
        }
        return output.toString();
    }
}
