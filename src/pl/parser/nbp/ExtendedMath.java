package pl.parser.nbp;

import java.util.List;

final class ExtendedMath {
    private ExtendedMath() {
    }

    static Float averageValue(List<Float> list) {
        float sum = 0;
        if (list.size() == 0)
            return sum;
        for (Float element : list) {
            sum += element;
        }
        return sum / list.size();
    }

    static Float standardDeviation(List<Float> list) {
        float sum = 0;
        if (list.size() == 0)
            return sum;
        float average = averageValue(list);
        for (Float element : list) {
            sum += Math.pow(element - average, 2);
        }
        return (float) Math.sqrt(sum / list.size());
    }
}
